#include "dsp_test.h"

#define SEMIHOSTING 1				//使能打印


//支持向量机实例化结构体，所有参数可以通过python生成
arm_svm_polynomial_instance_f32 params;


//通过使用scikit-learn包和一些随机输入数据对SVM分类器进行训练生成的参数
#define NB_SUPPORT_VECTORS 11  //向量个数


//向量空间的维数
#define VECTOR_DIMENSION 2

const float32_t dualCoefficients[NB_SUPPORT_VECTORS]={-0.01628988f, -0.0971605f,
  -0.02707579f,  0.0249406f,   0.00223095f,  0.04117345f,
  0.0262687f,   0.00800358f,  0.00581823f,  0.02346904f,  0.00862162f}; /* Dual coefficients */

const float32_t supportVectors[NB_SUPPORT_VECTORS*VECTOR_DIMENSION]={ 1.2510991f,   0.47782799f,
 -0.32711859f, -1.49880648f, -0.08905047f,  1.31907242f,
  1.14059333f,  2.63443767f, -2.62561524f,  1.02120701f,
 -1.2361353f,  -2.53145187f,
  2.28308122f, -1.58185875f,  2.73955981f,  0.35759327f,
  0.56662986f,  2.79702016f,
 -2.51380816f,  1.29295364f, -0.56658669f, -2.81944734f}; /* Support vectors */


//类A标识为“0”，类B标识为“1”
const int32_t   classes[2]={0,1};


void dsp_test(void)
{
		/* 输入数据 */
	float32_t in[VECTOR_DIMENSION];
	
	/* 分类器结果 */
	int32_t result;
	
	
	
		//初始化支持向量机实例化结构体
	arm_svm_polynomial_init_f32(&params,	//实例化结构体
		NB_SUPPORT_VECTORS,					//向量个数
		VECTOR_DIMENSION,					//向量维数
		-1.661719f,        					//截距
		dualCoefficients,					//双系数
		supportVectors,						//支持向量
		classes,							//类ID标识
		3,                 					//多项式次数
		1.100000f,         					//python scikit-learn包专用系数 
		0.500000f          					//python scikit-learn包专用系数
	);
	
	
	//输入测试数据，类A
	in[0] = 0.4f;
	in[1] = 0.1f;
	
	arm_svm_polynomial_predict_f32(&params, in, &result);
	
	//根据分类标识：如果分类器结果为A，则标识必为“0”
	#if defined(SEMIHOSTING)
	printf("Result = %d\n", result);
	#endif
	
	//输入测试数据，类B
	in[0] = 3.0f;
	in[1] = 0.0f;
	
	arm_svm_polynomial_predict_f32(&params, in, &result);
	
	//根据分类标识：如果分类器结果为A，则标识必为“1”
	#if defined(SEMIHOSTING)
	printf("Result = %d\n", result);
	#endif
	
}

