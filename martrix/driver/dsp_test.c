#include "dsp_test.h"

arm_matrix_instance_f32  S;
arm_matrix_instance_f32  S1;
arm_matrix_instance_f32  S2;
uint16_t nRows=5;
uint16_t nColumns=5;


static void dsp_test1(void)
{
	float32_t  pData[25]={17,24,1,8,15,
						23,5,7,14,16,
						4,6,13,20,22,
						10,12,19,21,3,
						11,18,25,2,9};

	float32_t  pData1[25]={3,3,3,3,3,
						3,3,3,3,3,
						3,3,3,3,3,
						3,3,3,3,3,
						3,3,3,3,3};	

	float32_t  pData2[25]={0,0,0,0,0,
						0,0,0,0,0,
						0,0,0,0,0,
						0,0,0,0,0,
						0,0,0,0,0};	
	
	u8 i=0;
	arm_mat_init_f32(&S,nRows,nColumns,pData);
	arm_mat_init_f32(&S1,nRows,nColumns,pData1);
	arm_mat_init_f32(&S2,nRows,nColumns,pData2);
	
	
	printf("����A=\r\n");
	for(i=0;i<25;i++)
	{
		printf("%f	",S.pData[i]);
		if(i%5==4)
		{
			printf("\r\n");
		}
	}
	
	printf("\r\n");
	printf("����B=\r\n");
	for(i=0;i<25;i++)
	{
		printf("%f	",S1.pData[i]);
		if(i%5==4)
		{
			printf("\r\n");
		}
	}
	
	arm_mat_add_f32(&S,&S1,&S2);
	printf("\r\n");
	printf("����A+B=\r\n");
	for(i=0;i<25;i++)
	{
		printf("%f	",S2.pData[i]);
		if(i%5==4)
		{
			printf("\r\n");
		}
	}
	
	arm_mat_mult_f32(&S,&S1,&S2);
	printf("\r\n");
	printf("����A*B=\r\n");
	for(i=0;i<25;i++)
	{
		printf("%f	",S2.pData[i]);
		if(i%5==4)
		{
			printf("\r\n");
		}
	}
	
	arm_mat_inverse_f32(&S,&S2);
	printf("\r\n");
	printf("����A�������=\r\n");
	for(i=0;i<25;i++)
	{
		printf("%f	",S2.pData[i]);
		if(i%5==4)
		{
			printf("\r\n");
		}
	}
	
}

static void dsp_test2(void)
{
	u8 i=0;
	
	float32_t  pData[25]={17,24,1,8,15,
						23,5,7,14,16,
						4,6,13,20,22,
						10,12,19,21,3,
						11,18,25,2,9};

	float32_t  pData1[25]={3,3,3,3,3,
						3,3,3,3,3,
						3,3,3,3,3,
						3,3,3,3,3,
						3,3,3,3,3};	

	float32_t  pData2[25]={0,0,0,0,0,
						0,0,0,0,0,
						0,0,0,0,0,
						0,0,0,0,0,
						0,0,0,0,0};	
	
	arm_mat_init_f32(&S,nRows,nColumns,pData);
	arm_mat_init_f32(&S1,nRows,nColumns,pData1);
	arm_mat_init_f32(&S2,nRows,nColumns,pData2);
	
	arm_mat_scale_f32(&S,0.1f,&S2);
	printf("\r\n");
	printf("����A*0.1=\r\n");
	for(i=0;i<25;i++)
	{
		printf("%f	",S2.pData[i]);
		if(i%5==4)
		{
			printf("\r\n");
		}
	}
	
	
	arm_mat_sub_f32(&S,&S1,&S2);
	printf("\r\n");
	printf("����A-B=\r\n");
	for(i=0;i<25;i++)
	{
		printf("%f	",S2.pData[i]);
		if(i%5==4)
		{
			printf("\r\n");
		}
	}
	
	
	arm_mat_trans_f32(&S,&S2);
	printf("\r\n");
	printf("����A��ת��=\r\n");
	for(i=0;i<25;i++)
	{
		printf("%f	",S2.pData[i]);
		if(i%5==4)
		{
			printf("\r\n");
		}
	}
}

void dsp_test(void)
{
	dsp_test1();
	dsp_test2();
}

