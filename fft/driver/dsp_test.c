#include "dsp_test.h"

arm_rfft_instance_f32 S;				//实序列傅里叶变换结构体
arm_cfft_radix4_instance_f32 S_CFFT;	//傅里叶变换结果复序列结构体

static float32_t pSrc[128]={0.0};   	//输入
static float32_t pDst[256]={0.0};		//输出
static float32_t out[128]={0};			//结果
void dsp_test(void)
{
	u8 i=0,j=0;
	for(i=0;i<128;i++)
	{
		pSrc[i]=1.6f+arm_sin_f32(100*PI*i/6400)+0.3f*arm_sin_f32(300*PI*i/6400)+0.5f*arm_sin_f32(700*PI*i/6400);
	}
	arm_rfft_init_f32(&S,&S_CFFT,128,0,1);	//初始化
	arm_rfft_f32(&S,pSrc,pDst);				//傅里叶变换
	for(j=0;j<127;j++)
	{
		arm_sqrt_f32(pDst[2*j]*pDst[2*j]+pDst[2*j+1]*pDst[2*j+1],&out[j]);//获得幅值
		if(j==0)
		{
			out[j]=out[j]/128;//直流分量需要特殊处理
		}
		else
		{
			out[j]=out[j]/64;//交流分量
		}
	}
	for(j=0;j<64;j++)
	{
		printf("%f\r\n",out[j]);
	}
}

