#include "dsp_test.h"
#include "systick.h"

arm_pid_instance_f32 S;		//定义PID结构体
float ref=2678./4095*3.3;	//ADC参考目标值：2678

void ad_da_init(void)
{
	//时钟配置
	rcu_periph_clock_enable(RCU_GPIOA);
	rcu_periph_clock_enable(RCU_AF);
	rcu_periph_clock_enable(RCU_ADC0);
	rcu_periph_clock_enable(RCU_DAC);
	
	rcu_adc_clock_config(RCU_CKADC_CKAPB2_DIV12);
	
	//接口配置
	gpio_init(GPIOA,GPIO_MODE_AIN,GPIO_OSPEED_50MHZ,GPIO_PIN_4);
	gpio_init(GPIOA,GPIO_MODE_AIN,GPIO_OSPEED_50MHZ,GPIO_PIN_6);
	
	//ADC输入配置
	adc_special_function_config(ADC0,ADC_CONTINUOUS_MODE,ENABLE);
	adc_data_alignment_config(ADC0,ADC_DATAALIGN_RIGHT);
	adc_channel_length_config(ADC0,ADC_REGULAR_CHANNEL,1);
	adc_regular_channel_config(ADC0,0,ADC_CHANNEL_6,ADC_SAMPLETIME_55POINT5);
	adc_external_trigger_config(ADC0,ADC_REGULAR_CHANNEL,DISABLE);
	
	adc_enable(ADC0);
	delay_ms(1);
	adc_calibration_enable(ADC0);
	
	//DAC输出配置
	dac_trigger_source_config(DAC0,DAC_TRIGGER_SOFTWARE);
	dac_trigger_enable(DAC0);
	dac_wave_mode_config(DAC0,DAC_WAVE_DISABLE);
	dac_output_buffer_enable(DAC0);
	
	dac_enable(DAC0);
	
	//PID参数初始化
	S.Kd=0;
	S.Ki=16;
	S.Kp=100;

	arm_pid_init_f32(&S,1);
}



volatile float adc_get=0.;

void dsp_test(void)
{
	 
	float32_t pid_num=0.;
	
	pid_num=arm_pid_f32(&S,ref-adc_get);	//PID处理，输入为误差信号

	if(pid_num>=4095)
	{
		pid_num=4095;
	}
    dac_data_set(DAC0, DAC_ALIGN_12B_R, (u16)pid_num);
       
    dac_software_trigger_enable(DAC0);
	
	adc_software_trigger_enable(ADC0,ADC_REGULAR_CHANNEL);
	if(adc_flag_get(ADC0,ADC_FLAG_EOC))
	{
		adc_flag_clear(ADC0,ADC_FLAG_EOC);
		adc_get = adc_regular_data_read(ADC0)/4095.*3.3;
	}
	
	printf("%f	%d	%d\r\n",adc_get,(u16)pid_num,adc_regular_data_read(ADC0));
}







