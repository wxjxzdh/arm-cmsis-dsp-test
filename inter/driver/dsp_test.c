#include "dsp_test.h"

void dsp_test(void)
{
	u8 i=0;
	float32_t data[128]={0.};
	for(i=0;i<128;i++)
	{
		data[i]=1.6f+arm_sin_f32(100*PI*i/6400)+0.3f*arm_sin_f32(300*PI*i/6400)+0.5f*arm_sin_f32(700*PI*i/6400);
		printf("%f\r\n",data[i]);
	}
}



//arm_spline_init_f32  parameter
static arm_spline_instance_f32  S;				//样条插值结构体
static arm_spline_type type=ARM_SPLINE_NATURAL;	//自然样条插值
static float32_t  x[32];						//原始数据x
static float32_t  y[32];						//原始数据y
static uint32_t n=32;							//原始数据个数
static float32_t  coeffs[3*(32-1)];
static float32_t  tempBuffer[2*(32-1)];
        

//arm_spline_f32   parameter
static float32_t  xq[128];
static float32_t  pDst[128];
static uint32_t blockSize=128;

#define num_tab 128/32

void interp_test(u8 mode)
{
        if(mode)
        {
                type=ARM_SPLINE_NATURAL;//自然样条插值
        }
        else
        {
                type=ARM_SPLINE_PARABOLIC_RUNOUT;//抛物样条插值
        }
        
        u8 i=0;
        for(i=0;i<32;i++)
        {
                x[i]=i*num_tab;
                y[i]=1.f+arm_sin_f32(100.f*PI*i/256.f+PI/3.f);
        }
        
        for(i=0;i<128;i++)
        {
                xq[i]=i;
        }
        
        arm_spline_init_f32(&S,type,x,y,n,coeffs,tempBuffer);
        arm_spline_f32(&S,xq,pDst,blockSize);
        
        printf("*****x********\r\n");
        for(i=0;i<32;i++)
        {
                printf("%f\r\n",x[i]);
        }
        printf("*****y********\r\n");
        for(i=0;i<32;i++)
        {
                printf("%f\r\n",y[i]);
        }
        
        
        printf("*****x1********\r\n");
        for(i=0;i<128;i++)
        {
                printf("%f\r\n",xq[i]);
        }
        
        printf("*****y1********\r\n");
        for(i=0;i<128;i++)
        {
                printf("%f\r\n",pDst[i]);
        }
}

