#include "dsp_test.h"

float32_t ia[128]={0.};		//输入A相电流
float32_t ib[128]={0.};		//输入B相电流

float32_t ia1[128]={0.};	//输出阿法轴电流
float32_t ib1[128]={0.};	//输出贝塔轴电流

void dsp_test(void)
{
	u8 i=0;
	
	for(i=0;i<128;i++)
	{
		ia[i]=arm_sin_f32(100*PI*i/6400);
		ib[i]=arm_sin_f32(100*PI*i/6400+PI*2/3);
	}
	
	for(i=0;i<128;i++)
	{
		arm_clarke_f32(ia[i],ib[i],&ia1[i],&ib1[i]);
	}
	
	for(i=0;i<128;i++)
	{
		printf("%f	%f	%f	%f\r\n",ia[i],ib[i],ia1[i],ib1[i]);
	}
}

