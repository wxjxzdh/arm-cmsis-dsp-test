#include "dsp_test.h"

arm_dct4_instance_f32 S;				//DCT 实例化结构体
arm_rfft_instance_f32  S_RFFT;			//实序列傅里叶变换实例化结构体
arm_cfft_radix4_instance_f32 S_CFFT;	//复数序列傅里叶变换实例化结构体
float32_t normalize=0.125;				//归一化因子

float32_t pInlineBuffer[128];			//输入输出
float32_t pState[128];					//state缓存
float32_t data[128]={0.f};





void dsp_test(void)
{
	u16 i=0;
	
	
	for(i=0;i<128;i++)
	{
		data[i]=1.5f+arm_sin_f32(100*PI*i/6400);
		printf("%f\r\n",data[i]);
		pInlineBuffer[i]=data[i];
	}
	
	
	arm_dct4_init_f32(&S,&S_RFFT,&S_CFFT,128,64,normalize);
	
	arm_dct4_f32(&S,pState,pInlineBuffer);
	
	for(i=0;i<128;i++)
	{
		printf("%f\r\n",pInlineBuffer[i]);
	}
}

