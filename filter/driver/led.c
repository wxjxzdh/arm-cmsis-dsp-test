#include "led.h"

void led_init(void)
{
	rcu_periph_clock_enable(RCU_GPIOE);
	
	gpio_init(GPIOE,GPIO_MODE_OUT_PP,GPIO_OSPEED_10MHZ,GPIO_PIN_10);
	gpio_init(GPIOE,GPIO_MODE_OUT_PP,GPIO_OSPEED_10MHZ,GPIO_PIN_11);
	gpio_init(GPIOE,GPIO_MODE_OUT_PP,GPIO_OSPEED_10MHZ,GPIO_PIN_12);
	gpio_init(GPIOE,GPIO_MODE_OUT_PP,GPIO_OSPEED_10MHZ,GPIO_PIN_13);
	
	led1=0;
	led2=0;
	led3=0;
	led4=0;
}

void led_on(void)
{
	led1=1;
	led2=1;
	led3=1;
	led4=1;
}

void led_off(void)
{
	led1=0;
	led2=0;
	led3=0;
	led4=0;
}



