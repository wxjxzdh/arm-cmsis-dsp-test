#include "dsp_test.h"

//arm_biquad_cascade_df1_init_f32  parameter（初始化结构体）
static arm_biquad_casd_df1_inst_f32  S;	//structure
static uint8_t numStages=2;   			//nums  of 2 order filter （2阶滤波器）

//5*numStages	xishu
static float32_t  pCoeffs[5*2]={1,2,1,1.9184107565980049,-0.92769312589129826,         
								1,2,1,1.8250960051409635,-0.83392686425555462};	
static float32_t  pState[4*2];			//4*numStages


//arm_biquad_cascade_df1_f32  parameter
static float32_t  pSrc[512];
static float32_t  pDst[512];
static uint32_t blockSize=1;

void dsp_test(void)
{
	u16 i=0;
	for(i=0;i<512;i++)
	{
		pSrc[i]=1.5f*arm_sin_f32(100*PI*i/6400)+0.5f*arm_sin_f32(900*PI*i/6400);
	}
	
	arm_biquad_cascade_df1_init_f32(&S,numStages,pCoeffs,pState);
	
	for(i=0;i<(512/blockSize);i++)
	{
		arm_biquad_cascade_df1_f32(&S,pSrc+i*blockSize,pDst+i*blockSize,blockSize);
	}
	
	
	printf("************\r\n");
	for(i=0;i<512;i++)
	{
		//Scale Values:                                                
		//0.0023205923233234451                                        
		//0.0022077147786478436
		printf("%f	%f\r\n",pSrc[i],pDst[i]*0.0023205923233234451f*0.0022077147786478436f);
	}
}

